## gs-flappy-bird
The purpose of this Project is to demonstrate how the [gs-project-manager](https://gitlab.com/godot-stuff/gs-project-manager) can be used to organize a number of existing Sources into a new Project. In this case we are using the demo game [Flappy Bird](https://godotengine.org/asset-library/asset/926), which has been cloned [here](https://gitlab.com/godot-stuff/gs-flappy-bird).

### Installation
Make sure you have gspm installed and loaded, then enter the following on the command line in some folder on your computer.

```bash
> git clone https://gitlab.com/godot-stuff/gs-flappy-bird.git
> cd gs-flappy-bird
> gspm install
> gspm run
```

This will pull down the latest project file, install the needed assets, and then run the project.

### Project Structure
This project is a little different in that we are not storing any Source code along with the [project.yml](https://gitlab.com/godot-stuff/gs-project-manager/wikis/project) file. Instead all of the Source is located in the Godot Asset Library.

This would be useful in a Scenario where you might want to pull all of your source for running a Build script specific for one type of OS.

### The Assets
If you look at the project.yml, you will see one asset entry for the game itself.

The gspm tool will install each asset in the order they are listed. In this case, only the one.

Pro Tip: Use the -v option with gspm to get a verbose listing of messages to see what is happening in the background.

### Repositories
The Project is pulling the Assets using their GIT repository locations. There is currently no facility for pulling anything from the Asset Library yet, although this might come in the future.
